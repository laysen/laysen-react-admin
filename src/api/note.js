import axios from './axios';
const api = {
  // 笔记分类列表
  note_cate_list(params = {}) {
    return axios.get('note/note_cate_list', params);
  },
  // 创建/修改笔记
  note_cate_submit(params = {}) {
    return axios.post('note/note_cate_submit', params);
  },
  note_list(params = {}) {
    return axios.get('note/note_list', params);
  },
  note_detail(params = {}) {
    return axios.get('note/note_detail', params);
  },
};
export default api;
