import Axios from 'axios';

Axios.defaults.headers['Cache-Control'] = 'no-cache';
Axios.defaults.headers.Pragma = 'no-cache';
Axios.defaults.headers.Expires = -1;
Axios.defaults.timeout = 10000; // 超时时间：10秒

//配置发送请求前的拦截器 可以设置token信息
Axios.interceptors.request.use(
  request => {
    request.headers.token = 'test_token';
    return request;
  },
  error => {
    return Promise.reject(error);
  }
);

// 配置响应拦截器
Axios.interceptors.response.use(
  res => {
    switch (res.status) {
      case 200:
        return Promise.resolve(res.data);
      case 404:
        return Promise.resolve({ ret: -1, msg: '请求资源不存在', data: {} });
      // return Promise.reject(res);
      default:
        return Promise.resolve({ ret: -1, msg: '请求资源报错', data: {} });
    }
  },
  error => {
    // Message({ type: 'error', message: '请求报错' });
    return Promise.resolve({ ret: -1, msg: error, data: {} });
    // return Promise.reject(error);
  }
);

const api = {
  prefix: '/api/admin/',
  formUrl(data) {
    let arr = [];
    for (let it in data) {
      arr.push(encodeURIComponent(it) + '=' + encodeURIComponent(data[it]));
    }
    return arr.join('&');
  },
  http(type = 'get', url, params) {
    return new Promise((resolve, reject) => {
      Axios[type](`${this.prefix}${url}`, this.formUrl(params)).then(
        response => {
          resolve(response);
        },
        err => {
          reject(err);
        }
      );
    });
  },
  get(url, params) {
    url += '?' + this.formUrl(params);
    return this.http('get', url);
  },
  post(url, params) {
    return this.http('post', url, params);
  },
};
export default api;
