import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Note from './note/Main';
import Bill from './bill/Main';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Route path="/note/" component={Note} />
        <Route path="/bill/" component={Bill} />
      </div>
    </BrowserRouter>
  );
}

export default App;
