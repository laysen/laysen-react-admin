import React from 'react';
import api from '@/api/note';

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  async noteCreate() {
    const res = await api.note_cate_submit();
    console.log(res)
  }
  render() {
    return (
      <div className="note-box">
        <div className="note-header"></div>
        <div className="note-main">
          <div className="note-left">
            <div className="note-create" onClick={this.noteCreate}>创建笔记</div>
          </div>
          <div className="note-nav"></div>
          <div className="note-content"></div>
        </div>
      </div >
    );
  }
}
export default Index;
