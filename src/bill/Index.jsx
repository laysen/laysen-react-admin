import React from 'react';

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
    return (
      <div className="note-box">
        <div className="note-header"></div>
        <div className="note-main">
          <div className="note-left">

          </div>
          <div className="note-nav"></div>
          <div className="note-content"></div>
        </div>
      </div >
    );
  }
}
export default Index;
