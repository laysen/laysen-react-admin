import React from 'react';
import { Route } from 'react-router-dom';

import './css/style.scss';
import Index from './Index';

class Note extends React.Component {
  render() {
    return (
      <div>
        <Route path="/bill/index" component={Index} />
      </div>
    );
  }
}

export default Note;
